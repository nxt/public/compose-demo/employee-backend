FROM openjdk:8-jdk-alpine AS dev

WORKDIR /app

# cache gradlew
COPY gradle gradle/
COPY gradlew ./
RUN ./gradlew -q --version

# cache dependencies
COPY src/main/kotlin/nxt/demo/employeebackend/EmployeeBackendApplication.kt \
   ./src/main/kotlin/nxt/demo/employeebackend/
COPY *.gradle ./
RUN ./gradlew -q --no-daemon classes

# Assemble application
COPY . ./
RUN ./gradlew -q --no-daemon assemble

ENV SPRING_PROFILES_ACTIVE development

ENTRYPOINT ["./gradlew", "--no-daemon"]
CMD ["bootRun"]

### BUILD STAGE

FROM openjdk:8-jre-alpine AS prod

WORKDIR /app

COPY --from=dev /app/build/libs/employee-backend-*.jar ./employee-backend.jar

ENV SPRING_PROFILES_ACTIVE production

ENTRYPOINT ["java", "-jar", "employee-backend.jar"]
