package nxt.demo.employeebackend.repository

import nxt.demo.employeebackend.model.Employee
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface EmployeeRepository : PagingAndSortingRepository<Employee, Long>
