package nxt.demo.employeebackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EmployeeBackendApplication

fun main(args: Array<String>) {
    runApplication<EmployeeBackendApplication>(*args)
}
