package nxt.demo.employeebackend.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Employee(
        @Id @GeneratedValue val id: Long = -1,
        var name: String
)
